// swift-tools-version:5.3
import PackageDescription

let package = Package(
    name: "Identomat",
    defaultLocalization: "en",
    platforms: [.iOS(.v10)],
    products: [
        .library(
            name: "Identomat",
            targets: ["identomat"]),
    ],
    dependencies: [
        .package(url: "https://github.com/stasel/WebRTC.git", from: "103.0.0")
    ],
    targets: [
        .binaryTarget(
            name: "identomat",
            path: "identomat.xcframework"
        )
    ]
)
